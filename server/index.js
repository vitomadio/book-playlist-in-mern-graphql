const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const db_url = require('./db_config/db');
const cors = require('cors');

const app = express();

// Allow cross origin requests
app.use(cors());

//connect to mlab database
mongoose.connect(db_url, {useNewUrlParser: true});
mongoose.connection.once('open', () => {
  console.log('connected to DB');
  
});

const PORT = process.env.PORT || 4000;

// Config Graphql
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));

app.listen(4000, () => {
  console.log('Now listening for requests from port:' + PORT);
});
require('dotenv').config();

let mode = process.env.NODE_ENV || 'development';

const db_url = {
  production: process.env.DB_HOST_PROD,
  development: process.env.DB_HOST_DEV
}[mode]

module.exports = db_url;
import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/queries';

function AddBook() {

  const { loading, error, data } = useQuery(getAuthorsQuery);
  const [addBook, results] = useMutation(addBookMutation);

  const [state, setState] = useState({
    name: null,
    genre: null,
    authorId: null
  });

  const submitForm = (e) => {
    e.preventDefault();
    addBook({
      variables: {
        name: state.name,
        genre: state.genre,
        authorId: state.authorId
      },
      refetchQueries: [{query:getBooksQuery}]
    });
    
  }

  return (
    <form id="add-book" onSubmit={(e) => submitForm(e)}>
      <div className="field">
        <label>Book name:</label>
        <input type="text" onChange={(e) => {
          let newState = {
            ...state,
            name: e.target.value
          }
          setState(newState);
        }} />
      </div>
      <div className="field">
        <label>Genre:</label>
        <input type="text" onChange={(e) => {
          let newState = {
            ...state,
            genre: e.target.value
          }
          setState(newState);
        }} />
      </div>
      <div className="field">
        <label>Author:</label>
        <select onChange={(e) => {
          let newState = {
            ...state,
            authorId: e.target.value
          }
          setState(newState);
        }}>
          <option>Select author</option>
          {data ? data.authors.map(author => <option key={author.id} value={author.id}>{author.name}</option>) : "<option disabled>Loading Authors...</option>"}
        </select>
      </div>
      <button>+</button>
    </form>
  );
}

export default AddBook;
import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { getBookQuery } from '../queries/queries';

function BookDetails({ bookId }) {

  const { error, loading, data } = useQuery(getBookQuery, {
    variables: { id: bookId }
  });

  return (
    <div id="book-details">
      <p>Output book details</p>
      {
        loading && <h2>Loading...</h2> ||
        error && `Error! ${error}` ||
        data.book &&
        <div>
          <h2>{data.book.name}</h2>
          <p><strong>
            Genre:
          </strong> {data.book.genre}</p>
          <p>Author: {data.book.author.name}</p> 
          <ul className="other-books">
            {data.book.author.books.map(item => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
        </div>
      }
    </div>
  )
}

export default BookDetails;

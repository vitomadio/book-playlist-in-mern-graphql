import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { getBooksQuery } from '../queries/queries';
import BookDetails from './BootDetails';

function BookList() {

  const [bookId, setBookId] = useState();

  const { loading, error, data } = useQuery(getBooksQuery);
  return (
    <div>
      <ul id="book-list">
        {data ? data.books.map(book => <li key={book.id} onClick={() => {setBookId(book.id)}}>{book.name}</li>) : "Loading..."}
      </ul>
      <BookDetails bookId={bookId}/>
    </div>
  );
}

export default BookList;

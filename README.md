## Book Playlist width MERN, GraphQL and Apollo.
This is a basic book play list made in MERN and using GraphQl ORM. 
Feel free to clone this repo and play with it locally as follows.

```
$ cd server && npm install
$ cd client && npm install
```
* Create an Mlab or MongoDB Database.
* Create a new ***.env*** file inside your server folder.
* Add these enviroment variables inside your ***.env*** file.
```
DB_HOST_PROD=<your production db uri here...>
DB_HOST_DEV=<your development db uri here...>
```
**NOTE:** In case you're working with a single DB you can use the same **uri** for both environment variables.

* Start Node server (Backend).
```
$ cd server && npm run dev
```
* Start React App (Frontend).
```
$ cd client && npm start
```
And that's it my friends, hope you enjoy it.

